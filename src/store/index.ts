import { Store, applyMiddleware, createStore } from "redux";
import { createEpicMiddleware } from "redux-observable";

import InitialState from "./initialState";
import epic from "../epics";
import reducer from "../reducers";

export type AppStore = Store<InitialState>;

export default createStore<InitialState>(
  reducer,
  new InitialState(),
  applyMiddleware(createEpicMiddleware(epic)),
);
