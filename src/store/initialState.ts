import { List, Record } from "immutable";

export class Post extends Record({
  userId: 0,
  id: 0,
  title: "",
  body: "",
}) {
  id: number;
  userId: number;
  body: string;
  title: string;
}

export default class extends Record({
  posts: List(),
}) {
  posts: List<Post>;
}
