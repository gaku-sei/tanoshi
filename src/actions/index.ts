import { List } from "immutable";

import { Post } from "../store/initialState";

export const FETCH_POSTS = "FETCH_POSTS";
export const FETCH_POSTS_SUCCESS = "FETCH_POSTS_SUCCESS";
export const FETCH_POSTS_FAILURE = "FETCH_POSTS_FAILURE";

export interface FetchPostsAction {
  type: typeof FETCH_POSTS;
}

export interface FetchPostsSuccessAction {
  type: typeof FETCH_POSTS_SUCCESS;
  payload: List<Post>;
}

export interface FetchPostsFailureAction {
  type: typeof FETCH_POSTS_FAILURE;
}

export type AllActions = FetchPostsAction | FetchPostsSuccessAction | FetchPostsFailureAction;
