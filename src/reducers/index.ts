import * as actions from "../actions";
import InitialState from "../store/initialState";

export default (state: InitialState, action: actions.AllActions) => {
  switch (action.type) {
    case actions.FETCH_POSTS_SUCCESS:
      return state.set("posts", action.payload) as InitialState;

    default:
      return state;
  }
};
