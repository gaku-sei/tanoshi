import { List } from "immutable";

import * as actions from "../actions";
import { Post } from "../store/initialState";

export const fetchPosts = (): actions.FetchPostsAction => ({
  type: actions.FETCH_POSTS,
});

export const fetchPostsSuccess = (payload: List<Post>): actions.FetchPostsSuccessAction => ({
  type: actions.FETCH_POSTS_SUCCESS,
  payload,
});

export const fetchPostsFailure = (): actions.FetchPostsFailureAction => ({
  type: actions.FETCH_POSTS_FAILURE,
});
