import { Observable } from "rxjs";
import { ActionsObservable } from "redux-observable";
import { List } from "immutable";

import { AppStore } from "../store";
import * as actions from "../actions";
import * as actionCreators from "../actionCreators";
import { Post } from "../store/initialState";

export default (action$: ActionsObservable<actions.FetchPostsSuccessAction>, store: AppStore) =>
  action$
    .ofType(actions.FETCH_POSTS)
    .flatMap(() => Observable.fromPromise(fetch("https://jsonplaceholder.typicode.com/posts") as Promise<Response>))
    .flatMap(response => Observable.fromPromise(response.json() as Promise<Post[]>))
    .map(posts => actionCreators.fetchPostsSuccess(List(posts)));
