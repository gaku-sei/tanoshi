import * as React from "react";
import { Dispatch, bindActionCreators } from "redux";
import { connect } from "react-redux";
import { List } from "immutable";

import { AppStore } from "../../store";
import { fetchPosts } from "../../actionCreators";
import InitialState, { Post } from "../../store/initialState";

interface StateProps {
  posts: List<Post>;
}

interface DispatchProps {
  actions: {
    posts: {
      fetch: typeof fetchPosts;
    };
  };
}

type Props = StateProps & DispatchProps;

const Home = (props: Props) => {
  const { actions, posts } = props;

  return (
    <div>
      <h1>Posts</h1>
      <button onClick={actions.posts.fetch}>
        Fetch
      </button>
      {!posts.size ? null : (
        <ul>
          {posts.map((post: Post) => (
            <li key={post.id}>{post.title}</li>
          ))}
        </ul>
      )}
    </div>
  );
};

export default connect<StateProps, DispatchProps, void>(
  ({ posts }: InitialState) => ({
    posts,
  }), (dispatch: Dispatch<AppStore>) => ({
    actions: {
      posts: {
        fetch: bindActionCreators(fetchPosts, dispatch),
      },
    },
  }),
)(Home);
